//
//  ViewController.h
//  ProjectA
//
//  Created by Sipre on 01/06/2013.
//  Copyright (c) 2013 Sipre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogIn.h"

@interface ViewController : UIViewController

- (IBAction)next:(id)sender;
@end
