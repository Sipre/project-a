//
//  Patient.h
//  ProjectA
//
//  Created by Sipre on 04/06/2013.
//  Copyright (c) 2013 Sipre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface Patient : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *patientId;
@property (nonatomic, strong) NSString *bithday;

- (id)init;
- (void)addPatient: (NSDictionary *)patient;
- (void)editPatient:(NSString *)withName withValueForKey:(NSObject *)valueForKey forKey:(NSString *)forKey;
- (NSArray *)searchPatient:(NSString *)withName;
- (void) deletePatient:(NSString *)withName;

@end
