//
//  Patients.m
//  ProjectA
//
//  Created by Sipre on 04/06/2013.
//  Copyright (c) 2013 Sipre. All rights reserved.
//

#import "Patients.h"

@interface Patients ()

@end

@implementation Patients

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
