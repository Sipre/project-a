//
//  Patient.m
//  ProjectA
//
//  Created by Sipre on 04/06/2013.
//  Copyright (c) 2013 Sipre. All rights reserved.
//

#import "Patient.h"

@implementation Patient{
    NSManagedObjectContext *context;
    AppDelegate *appdelegate;
}

- (id)init {
    self = [super init];
    if (self) {
        appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appdelegate managedObjectContext];
    }
    return self;
}

- (void)addPatient: (NSDictionary *)patient{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Patient" inManagedObjectContext:context];
    NSManagedObject *newPatient = [[NSManagedObject alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:context];
    
    [newPatient setValue:[patient valueForKey:@"name"]        forKey:@"name"];
    [newPatient setValue:[patient valueForKey:@"quantity"]    forKey:@"quantity"];
    [newPatient setValue:[patient valueForKey:@"frecuency"]   forKey:@"frecuency"];
    [newPatient setValue:[patient valueForKey:@"duration"]    forKey:@"duration"];
    [newPatient setValue:[patient valueForKey:@"image"]       forKey:@"image"];
    
    NSError *error;
    [context save:&error];
    NSLog(@"Added patient");
}


- (void)editPatient:(NSString *)withName withValueForKey:(NSObject *)valueForKey forKey:(NSString *)forKey{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Patient" inManagedObjectContext:context];
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:entityDescription];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name like %@",withName];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    if(matchingData.count <= 0){
        NSLog(@"No patient with name %@ found",withName);
    }
    else{
        for (NSManagedObject *obj in matchingData) {
            [obj setValue:valueForKey forKey:forKey];
            [context refreshObject:obj mergeChanges:YES];
            NSLog(@"Patient with name %@ changed",withName);
        }
        [context save:&error];
    }
    NSLog(@"Patient edited");
}

- (NSArray *)searchPatient:(NSString *)withName{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Patient" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name like %@",withName];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    return matchingData;
}

- (void) deletePatient:(NSString *)withName{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Patient" inManagedObjectContext:context];
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:entityDescription];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name like %@",withName];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    if(matchingData.count <= 0){
        NSLog(@"No patient with name %@ found",withName);
    }
    else{
        for (NSManagedObject *obj in matchingData) {
            [context deleteObject:obj];
            NSLog(@"Patient with name %@ deleted",withName);
        }
        [context save:&error];
    }
}


@end
