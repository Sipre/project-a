//
//  LogIn.h
//  ProjectA
//
//  Created by Sipre on 04/06/2013.
//  Copyright (c) 2013 Sipre. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface LogIn : UIViewController{
 IBOutlet UIButton *newAccount;
}

@property (strong, nonatomic) IBOutlet UILabel *mainTitle;
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UILabel *messageError;

- (IBAction)login:(UIButton *)sender;
- (IBAction)newUser:(UIButton *)sender;
@end
