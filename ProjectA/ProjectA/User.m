//
//  User.m
//  ProjectA
//
//  Created by Sipre on 04/06/2013.
//  Copyright (c) 2013 Sipre. All rights reserved.
//

#import "User.h"

@implementation User{
    AppDelegate *appdelegate;
}
@synthesize context;

- (id)init {
    self = [super init];
    if (self) {
        appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        context = [appdelegate managedObjectContext];
    }
    return self;
}

- (void)addUser: (NSDictionary *)user{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    NSManagedObject *newUser = [[NSManagedObject alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:context];
    
    [newUser setValue:[user valueForKey:@"name"]        forKey:@"name"];
    [newUser setValue:[user valueForKey:@"quantity"]    forKey:@"quantity"];
    [newUser setValue:[user valueForKey:@"frecuency"]   forKey:@"frecuency"];
    [newUser setValue:[user valueForKey:@"duration"]    forKey:@"duration"];
    [newUser setValue:[user valueForKey:@"image"]       forKey:@"image"];
    
    NSError *error;
    [context save:&error];
    NSLog(@"Added user");
}


- (void)editUser:(NSString *)withName withValueForKey:(NSObject *)valueForKey forKey:(NSString *)forKey{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:entityDescription];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username like %@",withName];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    if(matchingData.count <= 0){
        NSLog(@"No user with name %@ found",withName);
    }
    else{
        for (NSManagedObject *obj in matchingData) {
            [obj setValue:valueForKey forKey:forKey];
            [context refreshObject:obj mergeChanges:YES];
            NSLog(@"User with name %@ changed",withName);
        }
        [context save:&error];
    }
    NSLog(@"User edited");
}

- (NSArray *)searchUser:(NSString *)withName{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username like %@",withName];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    return matchingData;
}

- (void) deleteUser:(NSString *)withName{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    NSFetchRequest *request = [NSFetchRequest new];
    [request setEntity:entityDescription];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username like %@",withName];
    [request setPredicate:predicate];
    NSError *error;
    NSArray *matchingData = [context executeFetchRequest:request error:&error];
    if(matchingData.count <= 0){
        NSLog(@"No user with name %@ found",withName);
    }
    else{
        for (NSManagedObject *obj in matchingData) {
            [context deleteObject:obj];
            NSLog(@"User with name %@ deleted",withName);
        }
        [context save:&error];
    }
}


@end
