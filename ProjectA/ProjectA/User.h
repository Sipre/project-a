//
//  User.h
//  ProjectA
//
//  Created by Sipre on 04/06/2013.
//  Copyright (c) 2013 Sipre. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface User : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSManagedObjectContext *context;

- (id)init;
- (void)addUser: (NSDictionary *)user;
- (void)editUser:(NSString *)withName withValueForKey:(NSObject *)valueForKey forKey:(NSString *)forKey;
- (NSArray *)searchUser:(NSString *)withName;
- (void) deleteUser:(NSString *)withName;

@end
